package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.demo.domain.Pessoa;

@Repository
public interface UserRepository extends JpaRepository<Pessoa, Long>{
	
	Pessoa findByName(String name);
	
	Pessoa findByEmail(String email);
}
