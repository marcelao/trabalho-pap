package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import com.example.demo.domain.Pessoa;

public interface UserService {

	Optional<Pessoa> findUserById(long id);
    
    Pessoa findByName(String name);   
     
    void updateUser(Pessoa user);
     
    boolean deleteUserById(long id);
    
    Pessoa findByEmail(String email);
    
    List<Pessoa> listAll();

	public boolean existingUser(Pessoa user);
	
	public boolean save(Pessoa user);
	
	public boolean verifyWhitespace(Pessoa user);
}
