package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.domain.Pessoa;
import com.example.demo.repository.UserRepository;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;
	
	public Optional<Pessoa> findUserById(long id) {
		return userRepository.findById(id);
	}
	
	public Pessoa findByEmail(String email) {
		return userRepository.findByEmail(email);
	}
	
	public Pessoa findByName(String name) {
		return userRepository.findByName(name);
	}
	
	public void saveUser(Pessoa user) {
		userRepository.save(user);
	}

	public void updateUser(Pessoa user) {
		
	}

	public boolean deleteUserById(long id) {
		if(findUserById(id) != null){
			userRepository.deleteById(id);
			return true;
		}
		return false;
	}
	
	public List<Pessoa> findAllUsers() {
		return null;
	}

	public void deleteAllUsers() {}

	public boolean existingUser(Pessoa user) {
		if(findByName(user.getName()) != null) {
			return true;
		}else if(findByEmail(user.getEmail()) != null) {
			return true;
		}
		return false;
	}
	
	public boolean save(Pessoa user) {
		if(!verifyWhitespace(user)) {
			if(!existingUser(user)) {
				userRepository.save(user);
				return true;
			}
		}		
		
		return false;
	}

	public boolean verifyWhitespace(Pessoa user) {
		if(user.getName() == "" || user.getEmail() == "") {
			return true;
		}
		return false;
	}

	public List<Pessoa> listAll() {
		return userRepository.findAll();
	}
}
