package com.example.demo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "pessoa")
public class Pessoa {
	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "name", nullable=false)
	private String name;
	@Column(name = "email", nullable=false)
	private String email;
	
	
	public Pessoa(String name, String email) {
		this.name = name;
		this.email = email;
	}
	
	public Pessoa() {}
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	
}
