package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Pessoa;
import com.example.demo.service.UserService;


@RestController
@RequestMapping(path = "/user")
public class UserController{
		
	@Autowired
	UserService userService;
	
	@GetMapping(path = "/list")
	public List<Pessoa> list() {		
		return userService.listAll();
	}
	
	@GetMapping
	public Optional<Pessoa> searchById(@RequestParam Long id) {
		return userService.findUserById(id);
	}
	
	@PostMapping
	public boolean saveUser(@RequestBody Pessoa user) {
		return userService.save(user);
	}
	
	@DeleteMapping
	public boolean deleteUser(@RequestParam Long id) {
		return userService.deleteUserById(id);
	}
}
